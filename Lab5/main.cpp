#include "hw3_gn.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <Eigen/Dense>

#define NR 753
#define NX 3

using namespace std;
using namespace Eigen;

// ellipse753.txt 数据 x y z
vector<double> xx;
vector<double> yy;
vector<double> zz;

class Solver1616 : public GaussNewtonSolver {
    double solve(ResidualFunction *f, // 目标函数
                 double *X,           // 输入作为初值，输出作为结果
                 GaussNewtonParams param = GaussNewtonParams(), // 优化参数
                 GaussNewtonReport *report = nullptr// 优化结果报告
    );
};

class Function : public ResidualFunction {
    int nR() const;
    int nX() const;
    void eval(double *R, double *J, double *X);
};

double Solver1616::solve(ResidualFunction *f,
                         double *X,
                         GaussNewtonParams param,
                         GaussNewtonReport *report) {

    bool exact_line_search = param.exact_line_search;
    double gradient_tolerance = param.gradient_tolerance;
    double residual_tolerance = param.residual_tolerance;
    int max_iter = param.max_iter;
    bool verbose = param.verbose;

    float step_length = 0;
    if (!exact_line_search)
        step_length = 1;

    double gradient_error = 1;
    double residual_error = 0;
    int iter = 0;

    while (iter <= max_iter) {
        // 分配R J 空间
        double *R = new double [NR];
        double **J = new double *[NR];
        for(int i=0; i<NR ;i++ ){
            J[i] = new double[NX];
        }

        f->eval(R,(double *)J,X);

        double *pp = new double [NR*NX];

        int index = 0;
        for(int t=0;t<NX;t++){
            for(int s=0;s<NR;s++){
                pp[index] =  *(*(J+s)+t);
                index ++;
            }
        }

        Map<Matrix<double, NR, NX >> A(pp);
        Map<Matrix<double, NR, 1>> B(R);
        Vector3d mm = A.colPivHouseholderQr().solve(B);


        for(int i=0;i<NX;i++){
            X[i] += step_length * mm[i];
        }

        gradient_error = pow(pow(mm[0],2) + pow(mm[1],2) + pow(mm[2], 2), 0.5);

        for(int i=0;i<NR;i++){
            residual_error += abs(R[i]);
        }

        if (!verbose) {
            cout << "-----------iteration report:------------" << endl;
            cout << "iteration at :" << iter << endl;
            cout << "delta_x is : [ " << mm[0] << " " << mm[1] << " " << mm[2] << "]"<< endl;
            cout << "current X is : [ " << X[0] << ", " << X[1] << ", " << X[2] << "]" << endl;
            cout << "gradient_error is :" << gradient_error << endl;
            cout << "residual_error is :" << residual_error << endl;
        }

        if(gradient_error <= gradient_tolerance)
            break;

        if(residual_error <= residual_tolerance)
            break;

        residual_error = 0;
        gradient_error = 0;

        iter++;

        // 销毁R J 空间
        delete []R;
        delete []J;
        delete []pp;
    }

    cout << "-------------" << endl;
      cout << "Stop type : " << endl;
    if(iter >= max_iter){
        cout << "STOP_FOR_MAX_ITERATIONS" << endl;
    }
    if(gradient_error < gradient_tolerance){
        cout << "STOP_GRAD_TOL" << endl;
    }
    if(residual_error < residual_tolerance){
        cout << "STOP_RESIDUAL_TOL" << endl;
    }

    cout << "Final iterations : " << iter << endl;

    return *X;
}


int Function::nR() const{

    return NR;
}

int Function::nX() const{

    return NX;
}


void Function::eval(double *R, double *J, double *X) {

    double x_pow_2[NX];
    for(int i=0;i<NX;i++){
        x_pow_2[i] = pow(X[i], 2); // A^2 B^2 C^2
    }

    for (int i = 0; i < NR; i++) {
        // Xi^2/A^2 + Yi^2/B^2 + Zi^2/C^2
        double sum_sum = pow(xx[i], 2) / x_pow_2[0] + pow(yy[i], 2) / x_pow_2[1] + pow(zz[i], 2) / x_pow_2[2];
        R[i] = - pow(sum_sum - 1, 2);
    }

    double x_pow_NX[NX];
    for(int i=0;i<NX;i++){
        x_pow_NX[i] = pow(X[i], NX); // A^NX B^NX C^NX
    }

    for (int i = 0; i < NR; i++) {
        for (int k = 0; k <= NX; k++) {
            double tmp = 0;
            switch (k) {
                case 0:
                    tmp = pow(xx[i],2); //Xi^2
                    break;
                case 1:
                    tmp = pow(yy[i],2); //Yi^2
                    break;
                case 2:
                    tmp = pow(zz[i],2); //Zi^2
                    break;
                default:
                    break;
            }

            double sum_sum_sum = pow(xx[i], 2) / x_pow_2[0] + pow(yy[i], 2) / x_pow_2[1] + pow(zz[i], 2) / x_pow_2[2];
            double **p = (double **)J;
            *(*(p+i)+k) = 2 * ( sum_sum_sum - 1) * (-2 * tmp / x_pow_NX[k]);
        }
    }
}

int main() {
    cout << "start of the executing" << endl;

    // 三维点云恢复
    // 最小二乘
    // min R = (i=1->n) ∑  (xx_i)^2/A^2 + (yy_i)^2/B^2 + (zz_i)^2/C^2 - 1 )^2
    // search X = [A B C]
    // 实现 Gauss Newton 法求解简单的非线性最小二乘问题

    string file_path = "/Users/quiescence/OneDrive/Vision/compu-photo/lab/Lab5/ellipse753.txt";
    ifstream fin(file_path);
    string line;
    char *ptr;
    if (fin) {
        while (getline(fin, line)) {
            string str1, str2, strNX;
            istringstream is(line);
            is >> str1 >> str2 >> strNX;
            xx.push_back(strtod(str1.c_str(), &ptr));
            yy.push_back(strtod(str2.c_str(), &ptr));
            zz.push_back(strtod(strNX.c_str(), &ptr));
        }
    } else {
        cout << " there is no such file in this working path! " << endl;
    }

    fin.clear();
    fin.close();

    // X 初始化
    double X[] = {1, 2, 4};
    cout << "----------------------" << endl;
    cout << "Initial X is : [" << X[0] << ", " << X[1] << ", " << X[2] << " ]" << endl;
    ResidualFunction *f = new Function();
    GaussNewtonSolver *solver = new Solver1616;
    GaussNewtonParams param = GaussNewtonParams();
    GaussNewtonReport *report;
    solver->solve(f, X, param, report);
    cout << "----------------------" << endl;
    cout << "Final X is : [" << X[0] << ", " << X[1] << ", " << X[2] << " ]" << endl;

    cout << "end of the executing" << endl;

    return 0;
}