#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char* argv[]) {
    Mat image = imread("/Users/quiescence/sync/OneDrive/Vision/compu-photo/lab/Lab1/opencv-logo.png");
    namedWindow("original");// 创建一个标题为 "hello" 的窗口
    imshow("original", image);

    //白色反转为黑色
    for(int i=0;i<image.rows;i++){
        for(int j=0;j<image.cols;j++){
            Vec3b &pixel = image.at<Vec3b>(i,j);
            if(Vec3b(255, 255, 255) == pixel){
                pixel = Vec3b(0,0,0);
            }
        }
    }
    namedWindow("changed");
    imshow("changed", image);

    //反色
    Mat white(image.size(),image.type(),Scalar(255,255,255));
    image = white - image;
    namedWindow("white");
    imshow("white", image);

    //逆
    Mat L = 2.0*Mat::eye(256, 256, CV_32FC1);
    for(int i = 1;i < 256; ++i){
        L.at<float>(i-1,i) = -1;
        L.at<float>(i,i-1) = -1;
    }
    Mat Linv = L.inv();
    Mat result;
    normalize(Linv, result, 1.0, 0.0, CV_MINMAX);
    namedWindow("inv");
    imshow("inv", result);

    waitKey(0);
    destroyWindow("hello");
    destroyWindow("changed");
    destroyWindow("white");
    destroyWindow("inv");
    return 0;
}