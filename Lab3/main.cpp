#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Matrix {
public:
    // 稀疏矩阵存储
    vector<float> val_vec;
    vector<int> col_ind;
    vector<int> row_ptr;

    float at(int row, int col);

    void insert(float val, int, int col);

    void initializeFromVector(vector<int> rows,
                              vector<int> cols, vector<float> vals);
};


// 访问矩阵的行指标与列指标(row,col)所对应的值
float Matrix::at(int row, int col) {
    float val_out = 0;
    int col_pos = row_ptr[row];
    int col_pos_next = row_ptr[row + 1];
    if (col_pos >= 0) {
        for (int i = col_pos; i < col_pos_next; i++) {
            if (col_ind[i - 1] == (col + 1)) {
                val_out = val_vec[i - 1];
                break;
            }
        }
    }

    return val_out;
}

// 替换或插入
void Matrix::insert(float val, int row, int col) {
    int col_pos = row_ptr[row];
    int col_pos_next = row_ptr[row + 1];
    if (col_pos >= 0) {
        int i;
        for (i = col_pos; i < col_pos_next; i++) {
            if (col_ind[i - 1] == (col + 1)) {
                val_vec[i - 1] = val;
                return;
            }
            if (col_ind[i - 1] > (col + 1)) {
                break;
            }
        }
        col_ind.insert(col_ind.begin() + i - 1, col + 1);
        val_vec.insert(val_vec.begin() + i - 1, val);
        for (int k = row + 1; k < row_ptr.size(); k++) {
            row_ptr[k] = row_ptr[k] + 1;
        }

    }

    return;
}


void Matrix::initializeFromVector(vector<int> rows,
                                  vector<int> cols, vector<float> vals) {
    val_vec = vals;
    col_ind = cols;
    row_ptr = rows;

}

vector<float> Gauss_Seidel_Method(Matrix A, vector<float> b) {
    int n = A.row_ptr.size() - 1;
    vector<float> x(n, 0);
    vector<float> x_n_k(n, 0); //x^(k+1)
    vector<float> x_k(n, 0); //x^(K)
    while (1) {
        x_k = x_n_k;
        for (int k = 0; k < n; k++) {
            x_n_k[k] = 0;
        }

        for (int i = 0; i < n; i++) {
            float sum_i = 0, sum_j = 0, aii = 0;

            aii = A.at(i, i);

            for (int j = 0; j < i; j++) {
                sum_i += A.at(i, j) * x_k[j];
            }

            for (int j = n - 1; j > i; j--) {
                sum_j += A.at(i, j) * x_k[j];
            }

            x_n_k[i] = (b[i] - sum_i - sum_j) / aii;
        }

        // 收敛 停止迭代
        float error = 0;
        for (int i = 0; i < n; i++) {
            error += pow(x_n_k[i] - x_k[i], 2);
        }
        error = pow(error, 0.5);
        if (error < 1e-6) {
            x = x_n_k;
            break;
        } else {
            error = 0;
        }
    }

    return x;
}

int main() {
    cout << "start of executing" << endl;
    // 存储稀疏矩阵存储方式 Compressed Row Storage
    // 矩阵行列指标均从0计数
    // row_ptr vector
    int r[7] = {1, 3, 6, 9, 13, 17, 20};
    // col_ind vector
    int c[19] = {1, 5, 1, 2, 6, 2, 3, 4, 1, 3, 4, 5, 2, 4, 5, 6, 2, 5, 6};
    // val vector
    float v[19] = {10, -2, 3, 9, 3, 7, 8, 7, 3, 8, 7, 5, 8, 9, 9, 13, 4, 2, -1};

    vector<int> rows(r, r + 7);
    vector<int> cols(c, c + 19);
    vector<float> vals(v, v + 19);

    Matrix sparse_matrix;
    sparse_matrix.initializeFromVector(rows, cols, vals);

    // row col 为从0开始的行指标与列指标
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            cout << sparse_matrix.at(i, j) << " |";
        }
        cout << endl;
    }

    // the original value.at(row,col) != 0
    // 替换原本不为0的坐标
    float val = 88;
    int row = 2;
    int col = 2;
    sparse_matrix.insert(val, row, col);
    cout << "-------------------------------------------" << endl;
    cout << "After change the value in certain (row,col)" << endl;
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            cout << sparse_matrix.at(i, j) << " |";
        }
        cout << endl;
    }

    // the original value.at(row,col) = 0
    // 插入原本为0的坐标
    val = 99;
    row = 0;
    col = 5;
    sparse_matrix.insert(val, row, col);
    cout << "-------------------------------------------" << endl;
    cout << "After change the value in certain (row,col)" << endl;
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            cout << sparse_matrix.at(i, j) << " |";
        }
        cout << endl;
    }

    //------------稀疏矩阵的高斯赛达尔迭代法-----------------

    // Ax=b 求解
    // A =  | 10  -1   2   0 |
    //      | -1  11  -1   3 |
    //      |  2  -1  10  -1 |
    //      |  0   3  -1   8 |
    //
    // b = (6,25,-11,15)^T
    // expected x = [ 1, 2, -1 ,1]

    Matrix A;
    int rr[5] = {1, 4, 8, 12, 15};
    // col_ind vector
    int cc[14] = {1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 2, 3, 4};
    // val vector
    float vv[14] = {10, -1, 2, -1, 11, -1, 3, 2, -1, 10, -1, 3, -1, 8};

    vector<int> rrows(rr, rr + 5);
    vector<int> ccols(cc, cc + 14);
    vector<float> vvals(vv, vv + 14);

    A.initializeFromVector(rrows, ccols, vvals);

    float bb[4] = {6, 25, -11, 15};
    vector<float> b(bb, bb + 4);
    vector<float> x;
    x = Gauss_Seidel_Method(A, b);

    cout << "-------------------------------------------" << endl;
    cout << "A Matrix is: " << endl;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            cout << A.at(i, j) << " |";
        }
        cout << endl;
    }
    cout << "-------------------------------------------" << endl;
    cout << "b vector is: " << endl;
    cout << "[ ";
    for (int i = 0; i < b.size(); i++) {
        cout << b[i] << " ";
    }
    cout << "]" << endl;
    cout << "-------------------------------------------" << endl;
    cout << "Ax=b the answer x is: " << endl;
    cout << "[ ";
    for (int i = 0; i < x.size(); i++) {
        cout << x[i] << " ";
    }
    cout << "]" << endl;

    cout << "end of executing" << endl;

    return 0;
}