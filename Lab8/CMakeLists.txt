cmake_minimum_required(VERSION 3.9)
project(LAB8)

set(CMAKE_CXX_STANDARD 11)

add_executable(main main.cpp hw6_pa.h)

set(SOURCE_FILES main.cpp)
find_package(OpenCV REQUIRED)
target_link_libraries(main ${OpenCV_LIBS})

#LDFLAGS:  -L/usr/local/opt/opencv@2/lib
#CPPFLAGS: -I/usr/local/opt/opencv@2/include