//
// Created by quiescence on 2018/5/31.
//

#include "hw6_pa.h"

using namespace std;

class Panorama1616 : public CylindricalPanorama {
public:
    bool makePanorama(std::vector<cv::Mat> &img_vec, cv::Mat &img_out, double f);

    Mat cylindrical(Mat img_in, double f);

    Point2f homoPoint(const Point2f ori_Point, const Mat &transformMat);

    Mat stitch(Mat &A, Mat &B);

    void optimFuse(Mat &img_A, Mat &img_homo, Mat &img_all, Point2f left_top, Point2f left_bottom);
};

// 图像柱面投影
Mat Panorama1616::cylindrical(Mat img_in, double f) {
    int width = img_in.cols;
    int height = img_in.rows;

    Mat out(img_in.rows, img_in.cols, img_in.type());

    int centerX = width / 2;
    int centerY = height / 2;
    double theta;
    double pointX, pointY;

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            theta = asin((i - centerX) / f);
            pointX = int(f * tan((i - centerX) / f) + centerX);
            pointY = int((j - centerY) / cos(theta) + centerY);
            if (pointX >= 0 && pointX <= width && pointY >= 0 && pointY <= height) {
                out.at<Vec3b>(j, i) = img_in.at<Vec3b>(j, i);
            } else {
                out.at<Vec3b>(j, i)[0] = 0;
                out.at<Vec3b>(j, i)[1] = 0;
                out.at<Vec3b>(j, i)[2] = 0;
            }
        }
    }

    // 裁剪黑色区域
//    out = out(Rect(34, 0, 640 - 68, 340));
    return out;
}

//┌────────────────────────┐
//│                        │
//│    |x2 |        | x1 | │
//│                        │
//│    |y2 | =  H * | y1 | │
//│                        │
//│    | 1 |        |  1 | │
//│                        │
//└────────────────────────┘
Point2f Panorama1616::homoPoint(const Point2f originalPoint, const Mat &homoMat) {
    Mat originelP;
    // x1 y1 1
    originelP = (Mat_<double>(3, 1) << originalPoint.x, originalPoint.y, 1.0);
    // x2 y2 1
    Mat targetP;
    // Homography operation
    targetP = homoMat * originelP;
    float x = targetP.at<double>(0, 0) / targetP.at<double>(2, 0);
    float y = targetP.at<double>(1, 0) / targetP.at<double>(2, 0);
    return Point2f(x, y);
}


// 处理homo后两张图重合部分的区间
void Panorama1616::optimFuse(Mat &img_A, Mat &img_homo, Mat &img_all, Point2f left_top, Point2f left_bottom) {
    // 右图homo变换后得到img_homo，img_homo的左上角和左下角中较小的一个，即更左的一个点
    int start = MIN(left_top.x, left_bottom.x);
    // 左图宽度减img_homo左点，即重合区域的宽度
    double processWidth = img_A.cols - start;

    int rows = img_all.rows;
    int cols = img_A.cols;

    //img_A中像素的权重
    double alpha = 1;
    for (int i = start; i < cols; i++) {
        for (int j = 0; j < rows; j++) {
            //如果遇到图像img_homo中无像素的黑点，则完全拷贝img_A中的数据
            if (img_homo.at<Vec3b>(j, i)[0] == 0 && img_homo.at<Vec3b>(j, i)[1] == 0 &&
                img_homo.at<Vec3b>(j, i)[2] == 0) {
                alpha = 1;
            } else if (img_all.at<Vec3b>(j, i)[0] == 0 && img_all.at<Vec3b>(j, i)[1] == 0 &&
                       img_all.at<Vec3b>(j, i)[2] == 0) {
                alpha = 0;
            } else {
                //img_A中像素的权重，与当前处理点距重叠区域左边界的距离成正比
                alpha = (processWidth - (i - start)) / processWidth;
            }
            img_all.at<Vec3b>(j, i)[0] = img_A.at<Vec3b>(j, i)[0] * alpha + img_homo.at<Vec3b>(j, i)[0] * (1 - alpha);
            img_all.at<Vec3b>(j, i)[1] = img_A.at<Vec3b>(j, i)[1] * alpha + img_homo.at<Vec3b>(j, i)[1] * (1 - alpha);
            img_all.at<Vec3b>(j, i)[2] = img_A.at<Vec3b>(j, i)[2] * alpha + img_homo.at<Vec3b>(j, i)[2] * (1 - alpha);

        }
    }
}

// 拼接
// A为左图 B为右图
Mat Panorama1616::stitch(Mat &A, Mat &B) {
    Mat img_A = A;
    Mat img_B = B;

//    imshow("A", img_A);
//    waitKey();
//    imshow("B", img_B);
//    waitKey();

    // 转换成灰度图
    Mat gray_A, gray_B;
    cvtColor(img_A, gray_A, CV_RGB2GRAY);
    cvtColor(img_B, gray_B, CV_RGB2GRAY);

    // SIFT特征提取
    SiftFeatureDetector siftDetector(500);
    vector<KeyPoint> keyPoint_A, keyPoint_B;
    siftDetector.detect(gray_A, keyPoint_A);
    siftDetector.detect(gray_B, keyPoint_B);

    // 特征点描述
    SiftDescriptorExtractor siftDescriptor;
    Mat desc_A, desc_B;
    siftDescriptor.compute(gray_A, keyPoint_A, desc_A);
    siftDescriptor.compute(gray_B, keyPoint_B, desc_B);

    // 特征点匹配
    FlannBasedMatcher matcher;
    vector<DMatch> matchPoints;
    matcher.match(desc_A, desc_B, matchPoints);

    Mat matches;
    // 画出匹配
    drawMatches(img_A, keyPoint_A, img_B, keyPoint_B, matchPoints, matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
//    imshow("matches", matches);
//    waitKey();


    // 特征点对对齐，并转换为2D点
    vector<Point2f> img_p1, img_p2;
    for (int i = 0; i < matchPoints.size(); i++) {
        img_p1.push_back(keyPoint_A[matchPoints[i].queryIdx].pt);
        img_p2.push_back(keyPoint_B[matchPoints[i].trainIdx].pt);
    }

    // 根据匹配点生成变换矩阵，使用RANSAC方法
    Mat homoMat = findHomography(img_p2, img_p1, CV_RANSAC);

    Mat img_homo;
    // 计算右图变换后的四角对应的坐标
    Point2f originalPoint1 = Point2f(img_B.cols, 0);
    Point2f originalPoint2 = Point2f(img_B.cols, img_B.rows);
    // 右上角
    Point2f right_top = homoPoint(originalPoint1, homoMat);
    // 右下角
    Point2f right_bottom = homoPoint(originalPoint2, homoMat);

    Point2f originalPoint3 = Point2f(0, 0);
    Point2f originalPoint4 = Point2f(img_B.rows, 0);
    // 左上角
    Point2f left_top = homoPoint(originalPoint3, homoMat);
    // 左下角
    Point2f left_bottom = homoPoint(originalPoint4, homoMat);

    // 图像透视变换
    // 图像宽为左图变换后的右侧较大值
    warpPerspective(img_B, img_homo, homoMat,
                    Size(MAX(right_top.x, right_bottom.x), img_A.rows));

//    imshow("after homo change:", img_homo);
//    waitKey();

    //创建拼接后的图,需提前计算图的大小
    int img_all_width = img_homo.cols;  //取最右点的长度为拼接图的长度
    int img_all_height = img_A.rows;

    // 拼接的最终图像
    Mat img_all(img_all_height, img_all_width, CV_8UC3);
    img_all.setTo(0);

    // 将右图变换后拷贝到拼接图像对对应位置
    img_homo.copyTo(img_all(Rect(0, 0, img_homo.cols, img_homo.rows)));
    // 将左图拷贝到拼接图像对对应位置
    img_A.copyTo(img_all(Rect(0, 0, img_A.cols, img_A.rows)));

//    imshow("img_all", img_all);
//    waitKey();

    // 融合重合部分
    optimFuse(img_A, img_homo, img_all, left_top, left_bottom);
//    imshow("img_all", img_all);
//    waitKey();

    return img_all;
}


bool Panorama1616::makePanorama(std::vector<cv::Mat> &img_vec, cv::Mat &img_out, double f) {
//     图像总张数
    int num = int(img_vec.size());
    Mat out = cylindrical(img_vec[0], f);

    for (int i = 1; i < num; i++) {
        Mat in = cylindrical(img_vec[i], f);
        try{
            out = stitch(out, in);
        }
        catch (exception &e) {
            cout << "Standard exception: " << e.what() << endl;
        }

        imshow("stitch results", out);
        waitKey();
    }

    img_out = out;

//    Mat out1 = img_vec[0];
//    out1 = cylindrical(out1, f);
//    Mat out2 = img_vec[1];
//    out2 = cylindrical(out2, f);
//    Mat out12 = stitch(out1, out2);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama12.JPG", out12);

//    Mat out3 = img_vec[2];
//    out3 = cylindrical(out3, f);
//    Mat out4 = img_vec[3];
//    out4 = cylindrical(out4, f);
//    Mat out34 = stitch(out3, out4);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama34.JPG", out34);
//
//    Mat out5 = img_vec[4];
//    out5 = cylindrical(out5, f);
//    Mat out6 = img_vec[5];
//    out6 = cylindrical(out6, f);
//    Mat out56 = stitch(out5, out6);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama56.JPG", out56);
//
//    Mat out7 = img_vec[6];
//    out7 = cylindrical(out7, f);
//    Mat out8 = img_vec[7];
//    out8 = cylindrical(out8, f);
//    Mat out78 = stitch(out7, out8);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama78.JPG", out78);
//
//    Mat out9 = img_vec[8];
//    out9 = cylindrical(out9, f);
//    Mat out10 = img_vec[9];
//    out10 = cylindrical(out10, f);
//    Mat out910 = stitch(out9, out10);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama910.JPG", out910);
//
//    Mat out11 = img_vec[10];
//    out11 = cylindrical(out11, f);
//    Mat out122 = img_vec[11];
//    out122 = cylindrical(out122, f);
//    Mat out1112 = stitch(out11, out122);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama1112.JPG",
//            out1112);
//
//    Mat out1_4 = stitch(out12, out34);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama1234.JPG",
//            out1_4);
//
//    Mat out5_8 = stitch(out56, out78);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama5678.JPG",
//            out5_8);
//
//    Mat out9_12 = stitch(out910, out1112);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama9101112.JPG",
//            out9_12);
//
//    Mat out1_8 = stitch(out1_4, out5_8);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama1_8.JPG", out1_8);
//
//    Mat out5_12 = stitch(out5_8, out9_12);
//    imwrite("/Users/quiescence/Documents/GitHub/computational-photography/Lab8/panorama-data1/Panorama5_12.JPG",
//            out5_12);

    return false;
}

vector<Mat> readImages(cv::String pattern) {
    vector<cv::String> files;
    glob(pattern, files, false);
    vector<Mat> images;
    int count = int(files.size());
    for (size_t i = 0; i < count; i++) {
        images.push_back(imread(files[i]));
    }

    return images;
}


int main() {
    Panorama1616 P;
    vector<Mat> img_vec;
    Mat img_out;
    double f = 512.89;
    string dir = "/Users/quiescence/Documents/GitLab/computational-photography/Lab8/panorama-data1/";
    cv::String pattern = dir + "*.JPG";
    img_vec = readImages(pattern);

    if (P.makePanorama(img_vec, img_out, f)) {
        imwrite("/Users/quiescence/Documents/GitLab/computational-photography/Lab8/panorama-data1/Panorama.JPG",
                img_out);
        cout << "the panorama picture has been saved locally." << endl;
    } else {
        cout << "failure in making panorama." << endl;
    }

    return 0;
}