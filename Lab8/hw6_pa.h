//
// Created by quiescence on 2018/5/31.
//

#ifndef LAB8_HW6_PA_H
#define LAB8_HW6_PA_H
#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/legacy/legacy.hpp"
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace cv;

class CylindricalPanorama {
public:
    virtual bool makePanorama( std::vector<cv::Mat>& img_vec,cv::Mat& img_out,double f ) = 0;
};


vector<Mat> readImages(cv::String pattern);

#endif //LAB8_HW6_PA_H
