I = imread('panorama-data1/DSC01538.JPG');
[height, width, depth] = size(I);
A = I;
centerX = width / 2;
centerY = height / 2;
% alpha = pi / 4;
% f = width / (2 * tan(pi/4/2));
f = 512.89;
for i = 1 : width,
	for j = 1 : height,
		theta = asin((i - centerX) / f);
		pointX = int32(f * tan((i - centerX) / f) + centerX);
		pointY = int32((j - centerY) / cos(theta) + centerY);
		for k = 1 : depth,
			if pointX >= 1 && pointX <= width && pointY >= 1 && pointY <= height,
				A(j, i, k) = I(pointY, pointX, k);
			else
				A(j, i, k) = 0;
			end;
		end;
	end;
end;
subplot(1, 2, 1);
imshow(I);
subplot(1, 2, 2);
imshow(A);