#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;
using namespace cv;

int main(int argc, char* argv[]) {

    //中值滤波
    string srcImage = argv[1]; // input image filename with format extension
    string dstImage = argv[2]; // output image filename with format extension
    int w = atoi(argv[3]); // width of convolution kernel
    int h = atoi(argv[4]); // height of convolution kernel

    Mat image = imread(srcImage);
    Mat newImage;
    newImage.create(image.size(),image.type());

    int kernel_width;
    kernel_width = w * 2 + 1;
    int kernel_height;
    kernel_height = h * 2 + 1;

    for(int i=0;i<image.rows;i++){
        for(int j=0; j<image.cols;j++){
            // border process
            if((i-h)>0 && (i+h)< image.rows && (j-w)>0 && (j+w) < image.cols){
                vector<float> value_0;
                vector<float> value_1;
                vector<float> value_2;

                for(int m=(i-h);m<=(i+h);m++){
                    for(int n=(j-w);n<=(j+w);n++){
                        value_0.push_back(image.at<Vec3b>(m,n)[0]);
                        value_1.push_back(image.at<Vec3b>(m,n)[1]);
                        value_2.push_back(image.at<Vec3b>(m,n)[2]);
                    }
                }

                sort(value_0.begin(),value_0.end());
                sort(value_1.begin(),value_1.end());
                sort(value_2.begin(),value_2.end());

                newImage.at<Vec3b>(i,j)[0]= value_0[kernel_width*kernel_height/2];
                newImage.at<Vec3b>(i,j)[1]= value_1[kernel_width*kernel_height/2];
                newImage.at<Vec3b>(i,j)[2]= value_2[kernel_width*kernel_height/2];
            }
                // duplicate border pixel value
            else{
                newImage.at<Vec3b>(i,j) = image.at<Vec3b>(i,j);
            }
        }
    }

    imwrite(dstImage,newImage);
    cout << "end of executing" << endl;

    return 0;
}