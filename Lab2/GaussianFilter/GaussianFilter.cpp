#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char* argv[]) {

    //高斯滤波
    string srcImage = argv[1]; // input image filename with format extension
    string dstImage = argv[2]; // output image filename with format extension
    float sigma = atof(argv[3]); // sigma of gauss kernel

    const double PI = 3.1415926;
    int kernel_size = 5;
    int m = kernel_size/2;

    Mat kernel(kernel_size,kernel_size,CV_32F);
    float s = 2 * sigma * sigma;
    for (int i=0;i<kernel_size;i++){
        for(int j=0;j<kernel_size;j++){
            int x = i-m;
            int y = j-m;
            kernel.ptr<float>(i)[j] = exp(-(x*x+y*y)/s)/(PI*s);
        }
    }
    
    // cout << kernel << endl;

    Mat image = imread(srcImage);
    Mat newImage;
    newImage.create(image.size(),image.type());

    filter2D(image,newImage,-1,kernel);
    
    imwrite(dstImage,newImage);

    cout << "end of executing" << endl;
    return 0;
}