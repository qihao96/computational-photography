#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;
using namespace cv;


int main(int argc, char *argv[])
{

    //双边滤波
    string srcImage = argv[1]; // input image filename with format extension
    string dstImage = argv[2]; // output image filename with format extension
    float sigma_s = atof(argv[3]);
    float sigma_r = atof(argv[4]);

    Mat image = imread(srcImage);
    Mat newImage(image.rows, image.cols, image.type());

    float s_s = -0.5 / (sigma_s * sigma_s);
    float r_r = -0.5 / (sigma_r * sigma_r);

    int kernel_size = 25;
    int k = kernel_size / 2;

    vector<float> kernel(kernel_size * kernel_size);

    for (int i = 0; i < kernel_size; i++){
        for (int j = 0; j < kernel_size; j++){
            kernel[i * kernel_size + j] = exp((i * i + j * j) * s_s);
        }
    }

    float sum = 0.0f, k_k = 0.0f;
    for (int x = 0; x < image.rows; x++){
        unsigned char *pin = image.ptr<unsigned char>(x);
        unsigned char *pout = newImage.ptr<unsigned char>(x);

        for (int y = 0; y < image.cols; y++){
            int centerPix = y;
            // kernel window
            for (int i = -k; i <= k; i++){
                for (int j = -k; j <= k; j++){
                    int m = x + i;
                    int n = y + j;
                    if (x + i > -1 && y + j > -1 && x + i < image.rows && y + j < image.cols){
                        unsigned char value = image.at<unsigned char>(m, n);
                        float euklidDiff = kernel[(i + k) * kernel_size + (j + k)];
                        float intens = pin[centerPix] - value;
                        float factor = (float)exp(0.5 * r_r * intens) * euklidDiff;
                        sum += factor * value;
                        k_k += factor;
                    }
                }
            }
            pout[y] = (unsigned char) (sum / k_k);
            sum = 0.0f;
            k_k = 0.0f;
        }
    }

    imwrite(dstImage, newImage);

    // Mat newnewImage(image.rows, image.cols, image.type());
    // bilateralFilter(image,newnewImage,25,50,35);
    // imwrite("newImage.jpg", newnewImage);

    cout << "end of executing" << endl;
    return 0;
}