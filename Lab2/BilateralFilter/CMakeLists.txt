cmake_minimum_required(VERSION 3.9)
project(BilateralFilter)

set(CMAKE_CXX_STANDARD 11)

add_executable(BilateralFilter BilateralFilter.cpp)

set(SOURCE_FILES BilateralFilter.cpp)
find_package(OpenCV REQUIRED)
target_link_libraries(BilateralFilter ${OpenCV_LIBS})